package bunyaporn.pimpanit.kku.ac.th.alzheimercare;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by USER on 23/1/2561.
 */

public class BaseActivity extends AppCompatActivity {
    public void goTo(Class mClass){
        Intent intent = new Intent(this, mClass);
        startActivity(intent);
    }
}
