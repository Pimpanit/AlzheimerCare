package bunyaporn.pimpanit.kku.ac.th.alzheimercare.personal;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import bunyaporn.pimpanit.kku.ac.th.alzheimercare.R;
import bunyaporn.pimpanit.kku.ac.th.alzheimercare.firebase.Firebase;
import static bunyaporn.pimpanit.kku.ac.th.alzheimercare.personal.personal.*;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PersonalFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PersonalFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PersonalFragment extends Fragment {

    private ProgressDialog mProgress;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

// Inflate the layout for this fragment
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid(); // ดึงค่า uid
        DatabaseReference myRef = database.getReference(uid).child("patient");

        mProgress = new ProgressDialog(getActivity());
        mProgress.setMessage("Please wait...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);

        // แสดง progress bar
        mProgress.show();
        mProgress.dismiss();
        view = inflater.inflate(R.layout.fragment_personal, container, false);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String name = dataSnapshot.child("name").getValue(String.class);
                String address = dataSnapshot.child("address_no1").getValue(String.class);
                String birth = dataSnapshot.child("b_date").getValue(String.class);
                String tel = dataSnapshot.child("tel_mobile").getValue(String.class);
                String id = dataSnapshot.child("hn").getValue(String.class);
                /*String uri = dataSnapshot.child("image").getValue(String.class);*/
//                try {
//                    Glide.with(getActivity())
//                            .load(uri)
//                            .priority(Priority.IMMEDIATE)
//                            .into(imageView);
//
//                } catch (Exception e) {
//                    Log.d(TAG, "Error");
//                }

                createProfile(name, id, birth, address, tel);
                mProgress.setCancelable(true);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        /*getActivity().setTitle(getString(R.string.text_profile).toUpperCase());*/
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();

        return view;
    }

    private void createProfile(String name, String id, String birth, String address,
                               String tel) {

        TextView tvName = (TextView) view.findViewById(R.id.personal_name_tv);
        TextView tvID = (TextView) view.findViewById(R.id.personal_id_tv);
        TextView tvBirth = (TextView) view.findViewById(R.id.personal_birth_tv);
        TextView tvAddress = (TextView) view.findViewById(R.id.personal_address_tv);
        TextView tvTel = (TextView) view.findViewById(R.id.personal_tel_tv);
        /*imageView = (CircleImageView) view.findViewById(R.id.profile_image);*/

        tvName.setText(name);
        tvID.setText(id);
        tvBirth.setText(birth);
        tvAddress.setText(address);
        tvTel.setText(tel);

        setName = name;
        setID = id;
        setAddress = address;
        setTel = tel;
        setDate = birth;


    }

   /* // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            *//*throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");*//*
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    *//**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     *//*
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }*/
}