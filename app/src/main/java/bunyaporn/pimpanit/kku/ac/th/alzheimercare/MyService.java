package bunyaporn.pimpanit.kku.ac.th.alzheimercare;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.content.ContentValues.TAG;
import static bunyaporn.pimpanit.kku.ac.th.alzheimercare.NotificationStatus.isDinner;
import static bunyaporn.pimpanit.kku.ac.th.alzheimercare.NotificationStatus.isBreakfast;
import static bunyaporn.pimpanit.kku.ac.th.alzheimercare.NotificationStatus.isDinner;
import static bunyaporn.pimpanit.kku.ac.th.alzheimercare.NotificationStatus.isLunch;
import static bunyaporn.pimpanit.kku.ac.th.alzheimercare.NotificationStatus.isNotificationMassage;
import static bunyaporn.pimpanit.kku.ac.th.alzheimercare.NotificationStatus.isNotificationMedicine;
import static bunyaporn.pimpanit.kku.ac.th.alzheimercare.NotificationStatus.isNotificationOutside;
import static bunyaporn.pimpanit.kku.ac.th.alzheimercare.NotificationStatus.isNotificationQueue;

/**
 * Created by USER on 12/2/2561.
 */

public class MyService extends Service {

    private int number = 0;
    private boolean isRunning = false;
    private DatabaseReference myRef;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    String UID;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new Thread(new Runnable() {
            @Override
            public void run() {


                //Your logic that service will perform will be placed here
                //In this example we are just looping and waits for 1000 milliseconds in each loop.

                while (true) {
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                    }

                    if (isRunning) {


                        try {
                            UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
                        } catch (Exception e) {

                        }


                        final FirebaseDatabase database = FirebaseDatabase.getInstance();

                        final DatabaseReference root = database.getReference(UID);


                        root.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                /***************************************************************
                                 * Service medicine
                                 ***************************************************************
                                 */

                                DataSnapshot refMedicine = dataSnapshot.child("drug");

                                String value = (String) refMedicine.child("number").getValue();
                                number = Integer.parseInt(value);
                                Date date = new Date();
                                DateFormat timeFormat = new SimpleDateFormat("HH.mm");
                                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                                String currentTime = timeFormat.format(date);
                                String currentDate = dateFormat.format(date);

                                Double aDoubleCurrentTime = Double.parseDouble(currentTime);

                                for (int i = 0; i < number; i++) {

                                    DataSnapshot ref = refMedicine.child("list").child("id-" + (i + 1));

                                    String time = (String) ref.child("time").getValue();

                                    if (aDoubleCurrentTime > 0.00 && aDoubleCurrentTime <= 12.00) {
                                        if (contains(time, "เช้า")) {
                                            isBreakfast = true;
                                            isLunch = false;
                                            isDinner = false;
                                        }
                                    } else if (aDoubleCurrentTime > 12.00 && aDoubleCurrentTime <= 16.00) {
                                        if (contains(time, "กลางวัน")) {
                                            isBreakfast = false;
                                            isLunch = true;
                                            isDinner = false;
                                        }
                                    } else if (aDoubleCurrentTime > 16.00 && aDoubleCurrentTime <= 24.00) {
                                        if (contains(time, "เย็น")) {
                                            isBreakfast = false;
                                            isLunch = false;
                                            isDinner = true;

                                        }
                                    }

                                }

                                String timeBreakfast = (String) refMedicine.child("time").
                                        child("breakfast").getValue();
                                String timeLunch = (String) refMedicine.child("time").
                                        child("lunch").getValue();
                                String timeDinner = (String) refMedicine.child("time").
                                        child("dinner").getValue();


//                                if (isNotificationMedicine) {
//                                    if (NotificationStatus.isBreakfast) {
//                                        if (timeBreakfast.equals(currentTime)) {
//                                            showNotification("medicine", "แจ้งเตือนกินยา", "อย่าลืมกินยาตอนเช้า");
//                                            isNotificationMedicine = false;
//                                        }
//                                    }
//                                    if (isLunch) {
//                                        if (timeLunch.equals(currentTime)) {
//                                            showNotification("medicine", "แจ้งเตือนกินยา", "อย่าลืมกินยาตอนกลางวัน");
//                                            isNotificationMedicine = false;
//                                        }
//                                    }
//                                    if (isDinner) {
//                                        if (timeDinner.equals(currentTime)) {
//                                            showNotification("medicine", "แจ้งเตือนกินยา", "อย่าลืมกินยาตอนเเย็น");
//                                            isNotificationMedicine = false;
//                                        }
//                                    }
//                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError error) {
                                // Failed to read value
                                Log.w(TAG, "Failed to read value.", error.toException());
                            }
                        });

                    }
                }

                //Stop service once it finishes its task
                //stopSelf();
            }
        }).

                start();
        return Service.START_STICKY;
    }

    public boolean contains(String haystack, String needle) {
        haystack = haystack == null ? "" : haystack;
        needle = needle == null ? "" : needle;

        return haystack.toLowerCase().contains(needle.toLowerCase());
    }
}
