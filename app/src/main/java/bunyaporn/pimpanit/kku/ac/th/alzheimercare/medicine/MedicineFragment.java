package bunyaporn.pimpanit.kku.ac.th.alzheimercare.medicine;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import bunyaporn.pimpanit.kku.ac.th.alzheimercare.R;
import bunyaporn.pimpanit.kku.ac.th.alzheimercare.main.MainActivity;
import static bunyaporn.pimpanit.kku.ac.th.alzheimercare.NotificationStatus.isNotificationMassage;
import static bunyaporn.pimpanit.kku.ac.th.alzheimercare.NotificationStatus.isSetting;

public class MedicineFragment extends AppCompatActivity implements ActionBar.TabListener {

    // TODO: Rename and change types of parameters
    View view;
    private ViewPager tabsviewPager;
    private Tabsadapter mTabsAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_medicine);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(false);

        getSupportActionBar().setTitle(getString(R.string.text_medicine).toUpperCase());

        getSupportActionBar().setBackgroundDrawable(new
                ColorDrawable(getResources().getColor(R.color.colorGreen)));

        tabsviewPager = (ViewPager) findViewById(R.id.tabspager);

        mTabsAdapter = new Tabsadapter(getSupportFragmentManager());

        tabsviewPager.setAdapter(mTabsAdapter);

        ActionBar.Tab index = actionBar.newTab()
                .setText("รับประทานทุกวัน")
                .setTabListener(this);


        ActionBar.Tab video = actionBar.newTab()
                .setText("รับประทานเมื่อมีอาการ")
                .setTabListener(this);

        actionBar.addTab(index);
        actionBar.addTab(video);

        //This helps in providing swiping effect for v7 compat library
        tabsviewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // TODO Auto-generated method stub
                getSupportActionBar().setSelectedNavigationItem(position);

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.medicine, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            /*case R.id.fragment_medicine_setting:

//                DialogFragment newFragment = new TimePickerFragment();
//                newFragment.show(getActivity().getSupportFragmentManager(), "timePicker");

                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                Toast.makeText(this, "fragment Medicine Setting",
                        Toast.LENGTH_SHORT).show();

                return true;*/

//            case R.id.fragment_medicine_help:
//
//                Toast.makeText(this, "fragment Medicine help",
//                        Toast.LENGTH_SHORT).show();
//
//                return true;

            case android.R.id.home:
                Intent intent1 = new Intent(this, MainActivity.class);
                isNotificationMassage = false;
                isSetting = false;
                startActivity(intent1);
                break;
        }

        return false;
    }

    @Override
    public void onTabSelected(ActionBar.Tab selectedtab, FragmentTransaction ft) {
        tabsviewPager.setCurrentItem(selectedtab.getPosition()); //update tab position on tap
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

}
