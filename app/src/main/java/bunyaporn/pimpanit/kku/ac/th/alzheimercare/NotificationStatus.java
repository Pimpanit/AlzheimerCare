package bunyaporn.pimpanit.kku.ac.th.alzheimercare;

/**
 * Created by USER on 5/2/2561.
 */

public class NotificationStatus {
    public static boolean isBreakfast = false;
    public static boolean isLunch = false;
    public static boolean isDinner = false;

    public static boolean isNotificationMassage = false;
    public static boolean isNotificationOutside = true;
    public static boolean isNotificationMedicine = true;
    public static boolean isNotificationQueue = true;

    public static boolean isSetting;
}
