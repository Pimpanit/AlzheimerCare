package bunyaporn.pimpanit.kku.ac.th.alzheimercare.queue;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import bunyaporn.pimpanit.kku.ac.th.alzheimercare.R;
import bunyaporn.pimpanit.kku.ac.th.alzheimercare.main.MainFragment;

import static android.content.ContentValues.TAG;

public class QueueFragment extends Fragment {

    CompactCalendarView compactCalendarView;
    TextView tvDateCalender, tvTime, tvPlace, tvDate, tvDoctor;
    View view;
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy",
            Locale.getDefault());

    public QueueFragment() {
        // Required empty public constructor

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(UID).child("appointment");

        view = inflater.inflate(R.layout.fragment_queue, container, false);

        getActivity().setTitle(getString(R.string.text_queue).toUpperCase());
        ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new
                ColorDrawable(getResources().getColor(R.color.colorGreen)));


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String date = (String) dataSnapshot.child("date").getValue();
                String time = (String) dataSnapshot.child("time").getValue();
                String doctor = (String) dataSnapshot.child("doctor_name").getValue();
                String place = (String) dataSnapshot.child("place").getValue();

                createCalender(date, time, doctor, place);
            }

            @Override
            public void onCancelled(DatabaseError error) {

                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        return view;
    }

    // Change date to timestamp
    private long dateToTimestamp(String str_date) {
        Log.v("vv", str_date);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;

        try {
            date = (Date) formatter.parse(str_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Long timestamp = date.getTime();
        Log.d(TAG, "Today is " + timestamp);

        return timestamp;
    }

    private void createCalender(String date, String time, String doctor, String place) {

        tvDateCalender = (TextView) view.findViewById(R.id.queue_date);
        tvTime = (TextView) view.findViewById(R.id.queue_time_tv);
        tvDate = (TextView) view.findViewById(R.id.queue_date_tv);
        tvDoctor = (TextView) view.findViewById(R.id.queue_doctor_tv);
        tvPlace = (TextView) view.findViewById(R.id.queue_place_tv);
        compactCalendarView = (CompactCalendarView) view.findViewById(R.id.compactcalendar_view);
        // Set first day of week to Monday, defaults to Monday so calling setFirstDayOfWeek is not necessary
        // Use constants provided by Java Calendar class

        compactCalendarView.setUseThreeLetterAbbreviation(true);


        Long timestamp = dateToTimestamp(date);

        Event ev1 = new Event(Color.RED, timestamp, "test");
        compactCalendarView.addEvent(ev1, true);

        tvTime.setText(time);
        tvDoctor.setText(doctor);
        tvPlace.setText(place);
        tvDate.setText(date);
        tvDateCalender.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));

        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                List<Event> events = compactCalendarView.getEvents(dateClicked);

                Log.d(TAG, "Day was clicked: " + dateClicked + " with events " + events);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                Log.d(TAG, "Month was scrolled to: " + firstDayOfNewMonth);
                tvDateCalender.setText(dateFormatForMonth.format(firstDayOfNewMonth));
            }
        });
    }

    @Override
    public void onResume() {

        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    // handle back button
                    FragmentManager manager = getActivity().getSupportFragmentManager();
                    MainFragment mainFragment = new MainFragment();
                    manager.beginTransaction().replace(
                            R.id.content_main_fragment,
                            mainFragment,
                            mainFragment.getTag()
                    ).commit();
                    return true;

                }
                return false;
            }


        });
    }
}
