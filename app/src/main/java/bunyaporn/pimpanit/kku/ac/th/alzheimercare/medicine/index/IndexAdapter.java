package bunyaporn.pimpanit.kku.ac.th.alzheimercare.medicine.index;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import bunyaporn.pimpanit.kku.ac.th.alzheimercare.R;

/**
 * Created by USER on 5/2/2561.
 */

public class IndexAdapter extends RecyclerView.Adapter<IndexAdapter.ViewHolder> {
    private List<index> mIndices;
    private Context mContext;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mName;
        public TextView mDrugPer;
        public TextView mDrugWhen;
        public TextView mTime;
        public LinearLayout mImage;

        public ViewHolder(View view) {
            super(view);

            mName = (TextView) view.findViewById(R.id.medicine_name);
            mDrugPer = (TextView) view.findViewById(R.id.medicine_dos_weight);
            mDrugWhen = (TextView) view.findViewById(R.id.medicine_dos_time);
            mTime = (TextView) view.findViewById(R.id.medicine_time);
            mImage = (LinearLayout) view.findViewById(R.id.bg_drug);
        }
    }

    public IndexAdapter(Context context, List<index> dataset) {
        mIndices = dataset;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.recycler_view_row, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        index index = mIndices.get(position);

        viewHolder.mName.setText(index.getName());
        viewHolder.mDrugPer.setText(index.getDrugPer() + " เม็ด");
        viewHolder.mDrugWhen.setText(index.getDrugWhen());
        viewHolder.mTime.setText(index.getTime());
        viewHolder.mImage.setBackgroundResource(imageActive(index.getActive()));

    }

    @Override
    public int getItemCount() {
        return mIndices.size();
    }

    private int imageActive(String active) {

        int id = 0;
        if (active.equals("breakfast")) {
            id = R.drawable.bg_morning;
        } else if (active.equals("lunch")) {
            id = R.drawable.bg_afternoon;
        } else if (active.equals("dinner")) {
            id = R.drawable.bg_evening;
        }
        return id;
    }
}
