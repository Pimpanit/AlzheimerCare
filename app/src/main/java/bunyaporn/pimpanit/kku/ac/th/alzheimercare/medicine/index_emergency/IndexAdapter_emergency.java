package bunyaporn.pimpanit.kku.ac.th.alzheimercare.medicine.index_emergency;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import bunyaporn.pimpanit.kku.ac.th.alzheimercare.R;


/**
 * Created by USER on 13/2/2561.
 */

public class IndexAdapter_emergency extends RecyclerView.Adapter<IndexAdapter_emergency.ViewHolder>{

    private List<index_emergency> mIndices_emergency;
    private Context mContext_emergency;

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView mName_emergency;
        public TextView mDrugPer_emergency;
        public TextView mDrugWhen_emergency;
        public TextView mTime_emergency;
        public ImageView mImage_emergency;

        public ViewHolder(View view) {
            super(view);

            mName_emergency = (TextView) view.findViewById(R.id.medicine_name_emergency);
            mDrugPer_emergency = (TextView) view.findViewById(R.id.medicine_dos_weight_emergency);
            mDrugWhen_emergency = (TextView) view.findViewById(R.id.medicine_dos_time_emergency);
            mTime_emergency = (TextView) view.findViewById(R.id.medicine_time_emergency);
//            mImage_emergency = (ImageView) view.findViewById(R.id.img_active_emergency);
        }
    }

    public IndexAdapter_emergency(Context context, List<index_emergency> dataset) {
        mIndices_emergency = dataset;
        mContext_emergency = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext_emergency)
                .inflate(R.layout.recycler_view_row_emergency, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        index_emergency index = mIndices_emergency.get(position);

        holder.mName_emergency.setText(index.getName_emergency());
        holder.mDrugPer_emergency.setText(index.getDrugPer_emergency() + " เม็ด");
        holder.mDrugWhen_emergency.setText(index.getDrugWhen_emergency());
        holder.mTime_emergency.setText(index.getTime_emergency());
//        holder.mImage_emergency.setBackgroundResource(imageActive(index.getActive()));
    }

    @Override
    public int getItemCount() {
        return mIndices_emergency.size();
    }
}
