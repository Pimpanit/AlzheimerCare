package bunyaporn.pimpanit.kku.ac.th.alzheimercare.medicine.index;

/**
 * Created by USER on 5/2/2561.
 */

public class index {
    private String drug_name;
    private String beforeAfter;
    private String time_drug;
    private String drug_per;
    private String when_drug;
    private String active;

    public index(String drug_name, String drug_per, String time_drug, String when_drug, String active) {
        this.drug_name = drug_name;
        this.beforeAfter = beforeAfter;
        this.time_drug = time_drug;
        this.when_drug = when_drug;
        this.drug_per = drug_per;
        this.active = active;
    }

    public String getBeforeAfter() {

        return beforeAfter;
    }

    public void setBeforeAfter(String beforeAfter) {

        this.beforeAfter = beforeAfter;
    }

    public String getTime() {

        return time_drug;
    }

    public void setTime(String time_drug) {

        this.time_drug = time_drug;
    }

    public String getDrugPer() {
        return drug_per;
    }

    public void setDoseWeight(String drug_per) {

        this.drug_per = drug_per;
    }

    public String getDrugWhen() {

        return when_drug;
    }

//    public void setDrugWhen(String when_drug) {
//        this.when_drug = when_drug;
//    }

    public String getActive() {

        return active;
    }

    public void setActive(String active) {

        this.active = active;
    }

    public String getName() {

        return drug_name;
    }

    public void setName(String drug_name) {

        this.drug_name = drug_name;
    }
}
