package bunyaporn.pimpanit.kku.ac.th.alzheimercare.medicine.index_emergency;

/**
 * Created by USER on 13/2/2561.
 */

public class index_emergency {
    private String drug_name_emergency;
    private String beforeAfter_emergency;
    private String time_drug_emergency;
    private String drug_per_emergency;
    private String when_drug_emergency;
    private String active_emergency;

    public index_emergency(String drug_name_emergency, String drug_per_emergency, String time_drug_emergency, String when_drug_emergency, String active_emergency) {
        this.drug_name_emergency = drug_name_emergency;
        this.time_drug_emergency = time_drug_emergency;
        this.when_drug_emergency = when_drug_emergency;
        this.drug_per_emergency = drug_per_emergency;
        this.active_emergency = active_emergency;
    }

    public String getTime_emergency() {

        return time_drug_emergency;
    }

    public void setTime_emergency(String time_drug) {

        this.time_drug_emergency = time_drug;
    }

    public String getDrugPer_emergency() {

        return drug_per_emergency;
    }

    public void setDoseWeight_emergency(String drug_per_emergency) {

        this.drug_per_emergency = drug_per_emergency;
    }

    public String getDrugWhen_emergency() {

        return when_drug_emergency;
    }

    public String getActive_emergency() {

        return active_emergency;
    }

    public void setActive_emergency(String active_emergency) {

        this.active_emergency = active_emergency;
    }

    public String getName_emergency() {

        return drug_name_emergency;
    }

    public void setName_emergency(String drug_name_emergency) {

        this.drug_name_emergency = drug_name_emergency;
    }

}
