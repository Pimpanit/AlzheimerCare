package bunyaporn.pimpanit.kku.ac.th.alzheimercare.medicine;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.ContentValues.TAG;
import static bunyaporn.pimpanit.kku.ac.th.alzheimercare.NotificationStatus.isBreakfast;
import static bunyaporn.pimpanit.kku.ac.th.alzheimercare.NotificationStatus.isDinner;
import static bunyaporn.pimpanit.kku.ac.th.alzheimercare.NotificationStatus.isLunch;
import static bunyaporn.pimpanit.kku.ac.th.alzheimercare.NotificationStatus.isNotificationMedicine;


import bunyaporn.pimpanit.kku.ac.th.alzheimercare.R;
import bunyaporn.pimpanit.kku.ac.th.alzheimercare.medicine.index.IndexAdapter;
import bunyaporn.pimpanit.kku.ac.th.alzheimercare.medicine.index.index;

import static android.content.ContentValues.TAG;

public class MedicineSpecifyFragment extends Fragment implements View.OnClickListener{

    View view;
    String drug_name;
    String beforeAfter;
    String time_drug;
    String when_drug;
    String drug_per;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    String active;
    List<index> dataset;

    public MedicineSpecifyFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference(UID).child("drug");

        view = inflater.inflate(R.layout.fragment_medicine_specify, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        mRecyclerView.setHasFixedSize(true);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                isNotificationMedicine = true;
                dataset = new ArrayList<index>();

                for (DataSnapshot child_date : dataSnapshot.getChildren()) {

                    Object a = child_date.getValue();
                    Map<String, HashMap> child_type = (Map<String, HashMap>) child_date.getValue();
                    for (Map.Entry<String,HashMap> child_everyday : child_type.entrySet()) {
                        if (child_everyday.getKey().toString().equals("everyday")){
                            Map<String, HashMap> child_list_drug = (Map<String, HashMap>) child_everyday.getValue();
                            for (Map.Entry<String,HashMap> entry : child_list_drug.entrySet()) {
                                Map<String, HashMap> child_drug = (Map<String, HashMap>) entry.getValue();

                                for (Map.Entry drug : child_drug.entrySet()) {
                                    if (drug.getKey().toString().equals("drug_name")){
                                        drug_name = drug.getValue().toString();
                                    } else if (drug.getKey().toString().equals("drug_per")) {
                                        drug_per = drug.getValue().toString();
                                    } else if (drug.getKey().toString().equals("when_drug")) {
                                        when_drug = drug.getValue().toString();
                                    } else if (drug.getKey().toString().equals("time_drug")) {
                                        time_drug = drug.getValue().toString();
                                    }
                                    int g =1;

                                }
                                active = "";

                                if (isBreakfast) {
                                    if (contains(time_drug, "เช้า")) {
                                        active = "breakfast";
                                    }
                                } else if (isLunch) {
                                    if (contains(time_drug, "กลางวัน")) {
                                        active = "lunch";
                                    }
                                } else if (isDinner) {
                                    if (contains(time_drug, "เย็น")) {
                                        active = "dinner";
                                    }
                                }

                                index item = new index(drug_name, drug_per, time_drug, when_drug, active);
                                dataset.add(item);
                            }
                        }

                    }
                }

                mLayoutManager = new LinearLayoutManager(getActivity());
                mRecyclerView.setLayoutManager(mLayoutManager);

                mAdapter = new IndexAdapter(getActivity(), initIndex());
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        getActivity().setTitle("Medicine".toUpperCase());
        ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new
                ColorDrawable(getResources().getColor(R.color.colorGreen)));

        return view;
    }

    public boolean contains(String haystack, String needle) {
        haystack = haystack == null ? "" : haystack;
        needle = needle == null ? "" : needle;

        return haystack.toLowerCase().contains(needle.toLowerCase());
    }

    private List<index> initIndex() {

        return dataset;
    }

    @Override
    public void onClick(View view) {
        int itemPosition = mRecyclerView.getChildLayoutPosition(view);

        Toast.makeText(getActivity(), itemPosition + "", Toast.LENGTH_LONG).show();
    }
}
